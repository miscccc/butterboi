import discord
from discord.ext import commands
import os
import sys
import traceback



def get_prefix(bot, message):
    prefixes = ['!']
    # If we are in a guild, we allow for the user to mention us or use any of the prefixes in our list.
    return commands.when_mentioned_or(*prefixes)(bot, message)


initial_extensions = ['cogs.music', 'cogs.meme']
bot = commands.Bot(command_prefix=get_prefix, description='The butterest of lads')

if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            print('Failed to load extension {extension}.', file=sys.stderr)
            traceback.print_exc()


@bot.event
async def on_ready():
    print("Logged in as: ", bot.user.name)
    print(bot.user.id)
    print("----------")

token = ''
token_path = os.path.dirname(__file__)
token_path = os.path.join(token_path, 'token.txt')

with open(token_path, 'r') as f:
    token = f.readline()
    if "\r" or "\n" in token:
        print("Can't start up the bot because the token you gave me is wrong you fuck")
if token:
    while True:
        try:
            bot.loop.run_until_complete(bot.run(token.strip()))
        except BaseException:
            time.sleep(5)
else:
    print("Can't start up the bot because i wasnt given any token PROLLY BECAUSE THERE IS NONE IN token.txt BRADLEY")
